//
//  UILabel+Ext.swift
//  
//
//  Created by 이재성 on 2019/12/12.
//

import UIKit

public enum MarkDownType {
    case head1
    case head2
    case head3
    case head4
    case head5
    case italic
    case bold
    case callout
    case footnote
}

@available(iOS 11.0, *)
public extension UILabel {
    
    convenience init(_ text: String) {
        self.init()
        self.text = text
    }
    
    func text(_ text: String) {
        self.text = text
    }
    
    func align(_ alignment: NSTextAlignment) {
        self.textAlignment = alignment
    }
    
    func color(_ color: UIColor) {
        self.textColor = color
    }
    
    func elMarkdown(_ type: MarkDownType) {
        switch type {
        case .head1:
            self.font = .preferredFont(forTextStyle: .headline)
        case .head2:
            self.font = .preferredFont(forTextStyle: .subheadline)
        case .head3:
            self.font = .preferredFont(forTextStyle: .largeTitle)
        case .head4:
            self.font = .preferredFont(forTextStyle: .caption1)
        case .head5:
            self.font = .preferredFont(forTextStyle: .caption2)
        case .callout:
            self.font = .preferredFont(forTextStyle: .callout)
        case .footnote:
            self.font = .preferredFont(forTextStyle: .footnote)
        default:
            return
        }
    }
    
    func elMarkdown(_ type: MarkDownType, size: CGFloat) {
        switch type {
        case .bold:
            self.font = .boldSystemFont(ofSize: size)
        case .italic:
            self.font = .italicSystemFont(ofSize: size)
        default: return
        }
    }
}
