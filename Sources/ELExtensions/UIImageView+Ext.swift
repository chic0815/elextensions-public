//
//  UIImageView+Ext.swift
//  
//
//  Created by 이재성 on 2019/12/12.
//

import UIKit

extension UIImageView {
    convenience init(_ imageURL: String) {
        self.init()
        self.image = UIImage(named: imageURL)
    }
    
    func image(_ imageURL: String) {
        self.image = UIImage(named: imageURL)
    }
    
    func trimCorner(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func circle() {
        self.layer.cornerRadius = self.layer.frame.height / 2
        self.layer.masksToBounds = true
    }
    
    func border(_ color: UIColor, width: CGFloat = 1.0) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
}
