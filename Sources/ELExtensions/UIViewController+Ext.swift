//
//  UIViewController+Ext.swift
//  
//
//  Created by 이재성 on 2019/12/12.
//
import UIKit

@available(iOS 10.0, *)
public extension UIViewController {
    func toast(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func alert(error message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let actionDone = UIAlertAction(title: "Done", style: .cancel)
        alert.addAction(actionDone)
        self.present(alert, animated: true, completion: nil)
    }
}

