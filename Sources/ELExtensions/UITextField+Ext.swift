//
//  File.swift
//  
//
//  Created by 이재성 on 2019/12/12.
//

import UIKit

public extension UITextField {
    convenience init(_ placeholder: String) {
        self.init()
        self.placeholder = placeholder
    }
    
    func placeholder(_ placeholder: String) {
        self.placeholder = placeholder
    }
    
    func text(_ text: String) {
        self.text = text
    }
    
    func color(_ color: UIColor) {
        self.textColor = color
    }
    
    func align(_ alignment: NSTextAlignment) {
        self.textAlignment = alignment
    }
}
