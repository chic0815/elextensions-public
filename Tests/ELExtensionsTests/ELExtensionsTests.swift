import XCTest
@testable import ELExtensions

@available(iOS 11.0, *)
final class ELExtensionsTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ELExtensions().text, "Hello, World!")
    }

    func testLabel() {
        let title = UILabel("This is title")
        title.color(.black)
        title.align(.center)
        title.elMarkdown(.head1)
        
        let text = "This is title"
        let color = UIColor.black
        let alignment = NSTextAlignment.center
        
        XCTAssertEqual(title.text, text)
        XCTAssertEqual(title.textColor, color)
        XCTAssertEqual(title.textAlignment, alignment)
    }
    
    static var allTests = [
        ("testExample", testExample),
    ]
}
