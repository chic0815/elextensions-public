import XCTest

import ELExtensionsTests

var tests = [XCTestCaseEntry]()
tests += ELExtensionsTests.allTests()
XCTMain(tests)
